module.exports = {
    overrides: [
        {
            files: ['*.js'],
            extends: [
                './rules/javascript.js',
                './rules/promise.js',
            ],
            parserOptions: {
                ecmaVersion: 2022,
            },
            env: {
                es2022: true,
            },
        },
        {
            files: ['*.ts'],
            extends: [
                './rules/javascript.js',
                './rules/promise.js',
                './rules/typescript.js',
            ],
            parser: '@typescript-eslint/parser',
            parserOptions: {
                ecmaVersion: 2022,
            },
            plugins: [
                '@typescript-eslint',
            ],
            env: {
                es2022: true,
            },
        },
        {
            files: ['*.component.ts'],
            extends: [
                './rules/javascript.js',
                './rules/promise.js',
                './rules/typescript.js',
                './rules/angular-typescript.js',
            ],
            parser: '@typescript-eslint/parser',
            parserOptions: {
                ecmaVersion: 2022,
                sourceType: 'module',
            },
            plugins: [
                '@typescript-eslint',
            ],
            env: {
                es2022: true,
                browser: true,
            },
        },
        {
            files: ['*.spec.ts'],
            extends: [
                './rules/javascript.js',
                './rules/promise.js',
                './rules/typescript.js',
                './rules/angular-typescript.js',
            ],
            parser: '@typescript-eslint/parser',
            parserOptions: {
                ecmaVersion: 2022,
                sourceType: 'module',
            },
            plugins: [
                '@typescript-eslint',
            ],
            env: {
                es2022: true,
                jest: true,
            },
        },
        {
            files: ['*.component.html'],
            extends: [
                './rules/angular-template.js',
            ],
            parser: '@angular-eslint/template-parser',
            parserOptions: {
                ecmaVersion: 2022,
                sourceType: 'module',
            },
            plugins: [
                '@angular-eslint/template',
            ],
            env: {
                es2022: true,
                browser: true,
            },
        },
    ],
};
