module.exports = {
    overrides: [
        {
            files: ['*.js'],
            extends: [
                './rules/javascript.js',
                './rules/promise.js',
            ],
            parserOptions: {
                ecmaVersion: 2022,
            },
            env: {
                es2022: true,
            },
        },
    ],
};
