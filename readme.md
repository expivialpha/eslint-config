# Expivi eslint config

This repo contains the eslint configs used at Expivi.

## Available configs

| Config                             | Includes                            |
|------------------------------------|-------------------------------------|
| `@expivi`                          | es6 & promise                       |
| `@expivi/eslint-config/node`       | node, es6 & promise                 |
| `@expivi/eslint-config/vue`        | vue, es6 & promise                  |
| `@expivi/eslint-config/typescript` | typescript, es6 & promise           |
| `@expivi/eslint-config/angular`    | angular, typescript, es6 & promise  |

## Usage

```js
module.exports = {
  extends: '@expivi',
};
```
