module.exports = {
    root: true,
    extends: [
        './rules/javascript.js',
        './rules/node.js',
    ],
};
