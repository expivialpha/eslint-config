module.exports = {
    overrides: [
        {
            files: ['*.js'],
            extends: [
                './rules/javascript.js',
                './rules/promise.js',
            ],
            parserOptions: {
                ecmaVersion: 2022,
            },
            env: {
                es2022: true,
            },
        },
        {
            files: ['*.ts'],
            extends: [
                './rules/javascript.js',
                './rules/promise.js',
                './rules/typescript.js',
            ],
            parser: '@typescript-eslint/parser',
            parserOptions: {
                ecmaVersion: 2022,
            },
            plugins: [
                '@typescript-eslint',
            ],
            env: {
                es2022: true,
            },
        },
    ],
};
