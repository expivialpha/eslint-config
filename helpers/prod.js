module.exports = function prod(local = 'warn', production = 'error') {
    return ['development', 'production'].includes(process.env.NODE_ENV) ? production : local;
};
