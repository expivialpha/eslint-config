module.exports = {
    overrides: [
        {
            files: ['*.js'],
            extends: [
                './rules/javascript.js',
                './rules/promise.js',
            ],
            parserOptions: {
                ecmaVersion: 2022,
                sourceType: 'module',
            },
            env: {
                es2022: true,
                browser: true,
            },
        },
        {
            files: ['*.vue'],
            extends: [
                './rules/javascript.js',
                './rules/promise.js',
                './rules/vue.js',
            ],
            parser: 'vue-eslint-parser',
            parserOptions: {
                parser: '@babel/eslint-parser',
                sourceType: 'module',
                ecmaVersion: 2022,
            },
            env: {
                es2022: true,
                browser: true,
            },
        },
    ],
};
