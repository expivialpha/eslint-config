const prod = require('../../helpers/prod');

module.exports = {
    rules: {
        '@typescript-eslint/brace-style': [prod(), '1tbs', { allowSingleLine: true }],
        '@typescript-eslint/comma-dangle': [prod(), 'always-multiline'],
        '@typescript-eslint/comma-spacing': prod(),
        '@typescript-eslint/default-param-last': prod(),
        '@typescript-eslint/dot-notation': 'error',
        '@typescript-eslint/func-call-spacing': [prod(), 'never'],
        '@typescript-eslint/indent': ['error', 4, { SwitchCase: 1 }],
        '@typescript-eslint/keyword-spacing': prod(),
        '@typescript-eslint/lines-between-class-members': prod(),
        '@typescript-eslint/no-array-constructor': 'error',
        '@typescript-eslint/no-dupe-class-members': 'error',
        '@typescript-eslint/no-duplicate-imports': 'error',
        '@typescript-eslint/no-extra-parens': [prod(), 'all', {
            nestedBinaryExpressions: false,
            enforceForArrowConditionals: false,
            enforceForNewInMemberExpressions: false,
        }],
        '@typescript-eslint/no-loop-func': 'error',
        '@typescript-eslint/no-shadow': prod(),
        '@typescript-eslint/no-use-before-define': ['error', { functions: false }],
        '@typescript-eslint/no-useless-constructor': 'error',
        '@typescript-eslint/object-curly-spacing': [prod(), 'always'],
        '@typescript-eslint/padding-line-between-statements': [
            prod(),
            // Always require blank lines after directive (like 'use-strict'), except between directives
            {
                blankLine: 'always',
                prev: 'directive',
                next: '*',
            },
            {
                blankLine: 'any',
                prev: 'directive',
                next: 'directive',
            },
            // Always require blank lines after import, except between imports
            {
                blankLine: 'always',
                prev: 'import',
                next: '*',
            },
            {
                blankLine: 'any',
                prev: 'import',
                next: 'import',
            },
            // Always require blank lines before and after every sequence of variable declarations and export
            {
                blankLine: 'always',
                prev: '*',
                next: ['const', 'let', 'var', 'export'],
            },
            {
                blankLine: 'always',
                prev: ['const', 'let', 'var', 'export'],
                next: '*',
            },
            {
                blankLine: 'any',
                prev: ['const', 'let', 'var', 'export'],
                next: ['const', 'let', 'var', 'export'],
            },
            // Always require blank lines before and after class declaration, if, do/while, switch, try
            {
                blankLine: 'always',
                prev: '*',
                next: ['type', 'interface', 'if', 'class', 'for', 'do', 'while', 'switch', 'try', 'with'],
            },
            {
                blankLine: 'always',
                prev: ['type', 'interface', 'if', 'class', 'for', 'do', 'while', 'switch', 'try', 'with'],
                next: '*',
            },
            // Always require blank lines before return statements
            {
                blankLine: 'always',
                prev: '*',
                next: 'return',
            },
        ],
        '@typescript-eslint/quotes': [prod(), 'single'],
        '@typescript-eslint/require-await': prod(),
        '@typescript-eslint/return-await': 'error',
        '@typescript-eslint/semi': [prod(), 'always'],
        '@typescript-eslint/space-before-function-paren': [prod(), 'never'],
        '@typescript-eslint/space-infix-ops': ['error', { int32Hint: false }],
    },
};
