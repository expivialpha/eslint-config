const prod = require('../../helpers/prod');

module.exports = {
    rules: {
        '@typescript-eslint/adjacent-overload-signatures': prod(),
        '@typescript-eslint/array-type': [prod(), { default: 'array' }],
        '@typescript-eslint/await-thenable': 'error',
        '@typescript-eslint/ban-types': [prod(), {
            types: {
                Object: 'Use `{}` instead',
                String: {
                    message: 'Use `string` instead',
                    fixWith: 'string',
                },
                Number: {
                    message: 'Use `number` instead',
                    fixWith: 'number',
                },
                Boolean: {
                    message: 'Use `boolean` instead',
                    fixWith: 'boolean',
                },
            },
        }],
        '@typescript-eslint/consistent-type-assertions': prod(),
        '@typescript-eslint/consistent-type-definitions': [prod(), 'type'],
        '@typescript-eslint/member-delimiter-style': ['error', {
            singleline: {
                delimiter: 'comma',
                requireLast: false,
            },
            multiline: {
                delimiter: 'semi',
                requireLast: true,
            },
        }],
        '@typescript-eslint/no-array-constructor': prod(),
        '@typescript-eslint/no-extraneous-class': prod(),
        '@typescript-eslint/no-explicit-any': prod(),
        '@typescript-eslint/no-floating-promises': 'error',
        '@typescript-eslint/no-for-in-array': 'off',
        '@typescript-eslint/no-parameter-properties': 'off',
        '@typescript-eslint/no-unnecessary-qualifier': 'warn',
        '@typescript-eslint/no-unnecessary-type-arguments': 'warn',
        '@typescript-eslint/no-unused-vars': ['error', {
            vars: 'local',
            args: 'none',
            ignoreRestSiblings: false,
        }],
        '@typescript-eslint/no-var-requires': 'off', // Off for normal js files.
        '@typescript-eslint/no-useless-constructor': 'error',
        '@typescript-eslint/prefer-namespace-keyword': 'error',
        '@typescript-eslint/prefer-readonly': 'error',
        '@typescript-eslint/promise-function-async': ['error', {
            allowAny: true,
            allowedPromiseNames: [],
            checkArrowFunctions: true,
            checkFunctionDeclarations: true,
            checkFunctionExpressions: true,
            checkMethodDeclarations: true,
        }],
        '@typescript-eslint/restrict-plus-operands': 'error',
        '@typescript-eslint/unbound-method': ['error', {
            ignoreStatic: true,
        }],
        '@typescript-eslint/type-annotation-spacing': ['error', {
            before: false,
            after: true,
            overrides: {
                arrow: {
                    before: true,
                    after: true,
                },
            },
        }],
        '@typescript-eslint/triple-slash-reference': ['error', {
            types: 'prefer-import',
            path: 'always',
            lib: 'always',
        }],
        '@typescript-eslint/unified-signatures': 'warn',
        '@typescript-eslint/camelcase': 'off',
    },
};
