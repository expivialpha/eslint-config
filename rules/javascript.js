module.exports = {
    extends: [
        'eslint:recommended',
        './javascript/possibleErrors.js',
        './javascript/variables.js',
        './javascript/stylisticIssues.js',
        './javascript/bestPractices.js',
        './javascript/ecmascript6.js',
    ],
};
