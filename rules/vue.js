const prod = require('../helpers/prod');

module.exports = {
    extends: [
        '@vue/eslint-config-standard',
        'plugin:vue/strongly-recommended',
        './vue/recommended.js',
        './vue/stronglyRecommended.js',
        './vue/uncategorized.js',
    ],
    overrides: [
        {
            files: ['*.vue'],
            rules: {
                'indent': 'off',
                'semi': [prod(), 'always'],
                'comma-dangle': [prod(), 'always-multiline'],
                'quote-props': [prod(), 'consistent-as-needed'],
            },
        },
    ],
};
