module.exports = {
    extends: [
        'plugin:@angular-eslint/template/recommended',
        'plugin:@angular-eslint/template/process-inline-templates',
    ],
};
