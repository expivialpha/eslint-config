module.exports = {
    extends: [
        'plugin:node/recommended',
    ],
    rules: {
        'node/no-new-require': 'error',
        'node/no-mixed-requires': 'error',
        'no-console': 'off',
    },
};
